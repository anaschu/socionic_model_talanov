unit PartnerType1;

interface

uses
  Human,Aspect,IntoInfo2,Main, Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ButtonGroup,
  Vcl.ExtCtrls, IWVCLBaseControl, IWBaseControl, IWBaseHTMLControl, IWControl,
  IWCompRadioButton;


type
  TPartnerType1F = class(TForm)
    Panel1: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    IntNumEd: TEdit;
    LogNumEd: TEdit;
    etNumEd: TEdit;
    sensNumEd: TEdit;
    Memo1: TMemo;
    Button1: TButton;
    ExactlyTalRb: TRadioButton;
    ExactlyTBt: TRadioButton;
    ExtravertRB: TRadioButton;
    procedure ExtCanals();
    procedure massive();
    procedure Button1Click(Sender: TObject);
    procedure StepCompClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  PartnerType1F: TPartnerType1F;
  Time:integer;

implementation

{$R *.dfm}

procedure TPartnerType1F.ExtCanals();
begin
  intNum    := strtoInt(PartnerType1F.IntNumEd.Text);
  logNum    := strtoInt(PartnerType1F.logNumEd.Text);
  etNum     := strtoInt(PartnerType1F.etNumEd.Text);
  SensNum   := strtoInt(PartnerType1F.SensNumEd.Text);
end;

procedure TPartnerType1F.massive();
begin

  Hum.ModT[intNum-1].Name := '��������';
  Hum1.ModT[0].Name := '��������';

  Hum.ModT[etNum-1].Name := '������';
  Hum1.ModT[1].Name := '������';

  Hum.ModT[logNum-1].Name := '�����';
  Hum1.ModT[2].Name := '�����';

  Hum.ModT[sensNum-1].Name := '���������';
  Hum1.ModT[3].Name := '���������';
  end;

procedure TPartnerType1F.StepCompClick(Sender: TObject);
begin
//    ��� ��������� ���������� �� ������ ����� ���������� �������
 if ExtravertRB.Checked   then   ExtravertR:=true
 else  ExtravertR:=False;
 Setlength(Hum.ModT,4);
 Setlength(Hum1.ModT,4);
 Ostove(Hum);
 ExtravertR:=true;
 Ostove(Hum1);
 ExtCanals();
 massive();
  if ExtravertRB.Checked   then   ExtravertR:=true
 else  ExtravertR:=False;



 // �������� ������������� ����� (���� ���, ��
           // �������� �����)
 if (not Assigned(IntoInfo2F)) then
 IntoInfo2F:=TIntoInfo2F.Create(Self);
 //  �������� ��� ��������   �������������
  //   �� ����� �� ��������, � �� ��������.
 if  ExactlyTBt.Checked then ExactlyT:=true
 else ExactlyT:=false;

 // ���� ������� ������� "����� �� ��������"

  begin

  end;
  IntoInfo2F.Show;
  PartnerType1F.Visible:=False ;

end;

procedure TPartnerType1F.Button1Click(Sender: TObject);
begin
   Button1.Visible:=False;
   Button1.Enabled:=False;
   Memo1.Lines[1]:='� ��� � ������ ��� ��������������� � "���-��" �� ������ �����.  � ��� �� ����� ����������������, �� �������� � ���� ����.';
   Memo1.Lines.Add('��� �� �������, ����� ��� ����� ���������������� � �����?');
   Memo1.Lines.Add('����� ������ � ����� �������� ���� ����� ������, ����� - ������, � ��� �����.');
   Memo1.Lines.Add('���� ��� ����������� ������ ����� ������.');
   Memo1.Lines.Add('�� ������������                                                                ');
   Memo1.Lines.Add('����� ������� �� ������������ ������ ');
   Memo1.Lines.Add('����� ���������� ������� �� ��������� ����.');
   Memo1.Lines.Add('����� ����� �����- ���� ����� �����.');
   Memo1.Lines.Add('�� ������ �������� ���� ����� � ����� ���� ������ �������������� ��� ���� "1 2 3 4"');
end;

end.
