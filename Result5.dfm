object Result5F: TResult5F
  Left = 0
  Top = 0
  Caption = #1050#1086#1085#1077#1094' '#1088#1072#1073#1086#1090#1099' '#1087#1088#1086#1075#1088#1072#1084#1084#1099
  ClientHeight = 355
  ClientWidth = 656
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Progress: TLabel
    Left = 211
    Top = 71
    Width = 6
    Height = 13
    Caption = '0'
    Color = clAqua
    ParentColor = False
  end
  object Label1: TLabel
    Left = 45
    Top = 68
    Width = 111
    Height = 13
    Caption = #1054#1089#1090#1072#1083#1086#1089#1100' '#1074#1099#1087#1086#1083#1085#1080#1090#1100':'
    Color = clAqua
    ParentColor = False
  end
  object NotGoodLab: TLabel
    Left = 45
    Top = 103
    Width = 113
    Height = 13
    Caption = #1057#1090#1077#1087#1077#1085#1100' '#1085#1077#1091#1076#1086#1073#1089#1090#1074#1072' :'
    Color = clFuchsia
    ParentColor = False
  end
  object NotGoodValueLab: TLabel
    Left = 211
    Top = 111
    Width = 6
    Height = 13
    Caption = '0'
    Color = clFuchsia
    ParentColor = False
  end
  object TimeLab: TLabel
    Left = 43
    Top = 152
    Width = 90
    Height = 13
    Caption = #1087#1088#1086#1096#1083#1086' '#1074#1088#1077#1084#1077#1085#1080': '
    Color = clBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object TimeValueLab: TLabel
    Left = 211
    Top = 152
    Width = 6
    Height = 13
    Caption = '0'
    Color = clBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object Progress1: TLabel
    Left = 253
    Top = 71
    Width = 6
    Height = 13
    Caption = '0'
    Color = clAqua
    ParentColor = False
  end
  object NotGoodValueLab1: TLabel
    Left = 253
    Top = 111
    Width = 6
    Height = 13
    Caption = '0'
    Color = clFuchsia
    ParentColor = False
  end
  object TimeValueLab1: TLabel
    Left = 253
    Top = 152
    Width = 6
    Height = 13
    Caption = '0'
    Color = clBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object Label8: TLabel
    Left = 35
    Top = 84
    Width = 138
    Height = 13
    Caption = #1087#1086' '#1074#1085#1091#1090#1088#1077#1085#1085#1080#1084' '#1086#1097#1091#1097#1077#1085#1080#1103#1084
    Color = clAqua
    ParentColor = False
  end
  object Label9: TLabel
    Left = 35
    Top = 190
    Width = 164
    Height = 13
    Caption = #1087#1088#1072#1074#1072#1103' '#1082#1086#1083#1086#1085#1082#1072' '#1076#1083#1103' '#1076#1086#1085' '#1082#1080#1093#1086#1090#1072
  end
  object Label10: TLabel
    Left = 19
    Top = 171
    Width = 238
    Height = 13
    Caption = #1051#1077#1074#1072#1103' '#1082#1086#1083#1086#1085#1082#1072' '#1094#1080#1092#1088' '#1076#1083#1103' '#1074#1074#1077#1076#1077#1085#1086#1075#1086' '#1074#1072#1084#1080' '#1090#1080#1084#1072
  end
  object Label11: TLabel
    Left = 43
    Top = 22
    Width = 222
    Height = 13
    Caption = '4. '#1088#1077#1079#1091#1083#1100#1090#1072#1090' '#1074#1079#1072#1080#1084#1086#1076#1077#1081#1089#1090#1074#1080#1103' '#1058#1048#1084#1072' '#1089' '#1076#1086#1085#1086#1084
  end
  object IntAfter: TLabel
    Left = 288
    Top = 64
    Width = 43
    Height = 13
    Caption = #1048#1085#1090#1091#1094#1080#1103
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object LogAfter: TLabel
    Left = 288
    Top = 80
    Width = 36
    Height = 13
    Caption = #1051#1086#1075#1080#1082#1072
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object SensAfter: TLabel
    Left = 288
    Top = 96
    Width = 54
    Height = 13
    Caption = #1057#1077#1085#1089#1086#1088#1080#1082#1072
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object EtAfter: TLabel
    Left = 288
    Top = 112
    Width = 31
    Height = 13
    Caption = #1069#1090#1080#1082#1072
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object Label12: TLabel
    Left = 280
    Top = 24
    Width = 100
    Height = 13
    Caption = #1082#1072#1082' '#1087#1086#1084#1086#1075' '#1058#1048#1084' '#1076#1086#1085#1091
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object Button1: TButton
    Left = 280
    Top = 248
    Width = 137
    Height = 25
    Caption = #1079#1072#1082#1086#1085#1095#1080#1090#1100' '#1088#1072#1073#1086#1090#1091
    TabOrder = 0
    OnClick = Button1Click
  end
end
