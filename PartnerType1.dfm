object PartnerType1F: TPartnerType1F
  Left = 0
  Top = 0
  Caption = #1042#1099#1073#1086#1088' '#1090#1080#1084#1072', '#1089' '#1082#1086#1090#1086#1088#1099#1084' '#1073#1091#1076#1077#1090' '#1074#1079#1072#1080#1084#1086#1076#1077#1081#1089#1090#1086#1074#1072#1090#1100' '#1044#1086#1085' '#1050#1080#1093#1086#1090
  ClientHeight = 355
  ClientWidth = 656
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 8
    Top = 23
    Width = 353
    Height = 189
    Caption = 'Panel1'
    ParentBackground = False
    TabOrder = 0
    object Label3: TLabel
      Left = 8
      Top = 30
      Width = 69
      Height = 13
      Caption = #1048#1085#1090#1091#1080#1090#1080#1074#1085#1099#1081
    end
    object Label4: TLabel
      Left = 8
      Top = 64
      Width = 59
      Height = 13
      Caption = #1051#1086#1075#1080#1095#1077#1089#1082#1080#1081
    end
    object Label5: TLabel
      Left = 8
      Top = 100
      Width = 54
      Height = 13
      Caption = #1069#1090#1080#1095#1077#1089#1082#1080#1081
    end
    object Label6: TLabel
      Left = 8
      Top = 135
      Width = 56
      Height = 13
      Caption = #1057#1077#1085#1089#1086#1088#1085#1099#1081
    end
    object Label7: TLabel
      Left = 64
      Top = 8
      Width = 167
      Height = 13
      Caption = '1. '#1054#1095#1077#1088#1077#1076#1085#1086#1089#1090#1100' '#1092#1091#1085#1082#1094#1080#1081' '#1074' '#1058#1048#1052#1077
    end
    object IntNumEd: TEdit
      Left = 200
      Top = 30
      Width = 121
      Height = 21
      TabOrder = 0
      Text = '1'
    end
    object LogNumEd: TEdit
      Left = 200
      Top = 57
      Width = 121
      Height = 21
      TabOrder = 1
      Text = '2'
    end
    object etNumEd: TEdit
      Left = 200
      Top = 100
      Width = 121
      Height = 21
      TabOrder = 2
      Text = '3'
    end
    object sensNumEd: TEdit
      Left = 200
      Top = 132
      Width = 121
      Height = 21
      TabOrder = 3
      Text = '4'
    end
    object ExactlyTalRb: TRadioButton
      Left = 88
      Top = 188
      Width = 113
      Height = 17
      Caption = 'ExactlyTalRb'
      TabOrder = 4
    end
  end
  object StepComp: TButton
    Left = 312
    Top = 283
    Width = 336
    Height = 47
    Caption = #1089#1083#1077#1076#1091#1102#1097#1080#1081' '#1096#1072#1075' ('#1086#1090#1082#1088#1086#1077#1090#1089#1103' '#1085#1086#1074#1086#1077' '#1086#1082#1085#1086', '#1101#1090#1086' '#1079#1072#1082#1088#1086#1077#1090#1089#1103')'
    TabOrder = 1
    OnClick = StepCompClick
  end
  object Memo1: TMemo
    Left = 375
    Top = 23
    Width = 258
    Height = 234
    Lines.Strings = (
      'Memo1')
    TabOrder = 2
  end
  object Button1: TButton
    Left = 387
    Top = 47
    Width = 194
    Height = 78
    Caption = #1085#1072#1078#1084#1080#1090#1077' '#1076#1083#1103' '#1087#1086#1083#1091#1095#1077#1085#1080#1103' '#1089#1087#1088#1072#1074#1082#1080
    TabOrder = 3
    OnClick = Button1Click
  end
  object ExactlyTBt: TRadioButton
    Left = 8
    Top = 240
    Width = 353
    Height = 17
    Caption = ' '#1090#1086#1095#1085#1086' '#1087#1086' '#1090#1072#1083#1072#1085#1086#1074#1091', '#1073#1077#1079' '#1085#1072#1087#1086#1083#1085#1077#1085#1085#1086#1089#1090#1080' '#1072#1089#1087#1077#1082#1090#1086#1074' '#1080' '#1084#1072#1089#1086#1082
    TabOrder = 4
  end
  object ExtravertRB: TRadioButton
    Left = 8
    Top = 263
    Width = 113
    Height = 17
    Caption = #1069#1082#1090#1088#1072#1074#1077#1088#1090
    TabOrder = 5
  end
end
