unit IntoInfo2;

interface

uses
  Human, Aspect, BrainStrong3 ,Calculat4, Main, Winapi.Windows, Winapi.Messages,
  System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ButtonGroup,
  Vcl.ExtCtrls, IWVCLBaseControl, IWBaseControl, IWBaseHTMLControl, IWControl,
  IWCompRadioButton;

type
  TIntoInfo2F = class(TForm)
    LogCanEd: TEdit;
    CanalPan: TPanel;
    LogCanLab: TLabel;
    IntCanLab: TLabel;
    EticCanLab: TLabel;
    SensCanLab: TLabel;
    CanalPanel: TLabel;
    NamPanLab: TLabel;
    EtCanEd: TEdit;
    SenCanEd: TEdit;
    IntCanEd: TEdit;
    StepComp: TButton;
    Button1: TButton;
    Memo1: TMemo;
    Button2: TButton;
    procedure ExtCanals();
    procedure massive();
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FindAspect(aspname:string;Can:integer);
  private
    { Private declarations }
  public
    { Public declarations }


  end;

var
  IntoInfo2F: TIntoInfo2F;
  
implementation

{$R *.dfm}

procedure TIntoInfo2F.FindAspect(aspname:string;Can:integer);
var i:integer;
begin
for I := 0 to 3 do
    begin
      if Hum.ModT[i].Name=aspname then
       Hum.ModT[i].Inf.Info:=Can;
    end;

end;

procedure TIntoInfo2F.massive();
var
  I: Integer;
begin
  //  ��������� ������ ��������
   //  ��� ������� �������
  FindAspect('��������',intCan);
  FindAspect('�����', etCan);
  FindAspect('������', logCan);
  FindAspect('���������', sensCan);
   //    ��� ��������� ���������� �� ������ �����


  //��� ������� �� ������� �����

  //��� ��������� � �����
  


end;

procedure TIntoInfo2F.Button1Click(Sender: TObject);
begin
   Button1.Visible:=False;
   Button1.Enabled:=False;
   Memo1.Lines[1]:='����� ������� ��������:';
   Memo1.Lines.Add('��� �������� �� ������� �����?');
   Memo1.Lines.Add('������� ���������� ��������');
   Memo1.Lines.Add('�� ������ ������?');
   Memo1.Lines.Add('��������:');
   Memo1.Lines.Add('���� ���� ������ ���������� �������, ��:');
   Memo1.Lines.Add('�� ������� ����� �� ������ ������ �������� ������� ������. ��������� � ������ 10');
   Memo1.Lines.Add('�� ����� ��������� 0');
   Memo1.Lines.Add('�� ������ �������� ���� ����� � ����� ����');



end;

procedure TIntoInfo2F.Button2Click(Sender: TObject);
begin
  ExtCanals();
  massive();
  
IntoInfo2F.Visible:=False;
if (not Assigned(BrainStrong3F)) then   // �������� ������������� ����� (���� ���, ��
       BrainStrong3F:=TBrainStrong3F.Create(Self);    // �������� �����)
  if main.ExactlyT then
  begin

//    ��� ��������� ���������� �� ������ ����� ���������� �������
 BrainStrong3F.ExtCanals();
 BrainStrong3F.massive();
if (not Assigned(Calculat4F)) then   // �������� ������������� ����� (���� ���, ��
  Calculat4F:=TCalculat4F.Create(Self);    // �������� �����)
  Calculat4F.Show; // (��� BrainStrong3F.ShowModal) ����� �����
  BrainStrong3F.Visible:=False;
  end
  else
  BrainStrong3F.Show; // (��� BrainStrong3F.ShowModal) ����� �����


end;

procedure TIntoInfo2F.ExtCanals();
var i:integer;
begin
  intCan:= strtoInt(IntCanEd.Text);
  logCan := strtoInt(logCanEd.Text);
  EtCan  := strtoInt(EtCanEd.Text);
  SensCan := strtoInt(SenCanEd.Text);
  for I := 0 to 3 do
  Hum1.ModT[i].Inf.Info:=1;
end;






end.
