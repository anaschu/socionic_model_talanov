unit BrainStrong3;

interface

uses
Human, Aspect, Main, Calculat4, Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TBrainStrong3F = class(TForm)
    HeadPan: TPanel;
    LogHeadLab: TLabel;
    EtHeadLab: TLabel;
    SensHeadLab: TLabel;
    HeadPanel: TLabel;
    NamPan2Lab: TLabel;
    Label2: TLabel;
    LogHeadEd: TEdit;
    IntHeadEd: TEdit;
    EtHeadEd: TEdit;
    SenHeadEd: TEdit;
    Memo1: TMemo;
    Button1: TButton;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure massive();
    procedure ExtCanals();
    procedure FindAspect(aspname:string;Num:integer);


  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  BrainStrong3F: TBrainStrong3F;

implementation

{$R *.dfm}

procedure TBrainStrong3F.FindAspect(aspname:string;Num:integer);
var i:integer;
begin
for I := 0 to 3 do
    begin
      if Hum.ModT[i].Name=aspname then
       Hum.ModT[i].Strong:=Table[2,Num]
    end;

end;

procedure TBrainStrong3F.massive();
var i:integer;
begin
  FindAspect('��������',intHead);
  FindAspect('�����', etHead);
  FindAspect('������', logHead);
  FindAspect('���������', senHead);
   for I := 0 to 3 do
  Hum.ModT[i].Strong:=1;


end;

procedure TBrainStrong3F.ExtCanals();

begin
  intHead  := strtoInt(BrainStrong3F.IntHeadEd.Text);
  logHead  := strtoInt(BrainStrong3F.logHeadEd.Text);
  etHead   := strtoInt(BrainStrong3F.etHeadEd.Text);
  senHead  := strtoInt(BrainStrong3F.senHeadEd.Text);

end;

procedure TBrainStrong3F.Button1Click(Sender: TObject);
begin
   Button1.Visible:=False;
   Button1.Enabled:=False;
   Memo1.Lines[1]:='����� �������:';
   Memo1.Lines.Add('��� �������� ������ ������ (����)?');
   Memo1.Lines.Add('������� ���������� ����� ����������� ��');
   Memo1.Lines.Add('���������� �� ������ �������?');
   Memo1.Lines.Add('��������:');
   Memo1.Lines.Add('���� ��� ���� ������ ����������, ��:');
   Memo1.Lines.Add('��� ��� �� ����������� ��� ( � �� ����� ���� �������������, � �������� ������������� ������� ����� ������, ��� ������), �� �� ������  �� ����� ������ � ������ �� 10');
   Memo1.Lines.Add('�� �� ����� �� ����, ��������� 0');
end;

procedure TBrainStrong3F.Button2Click(Sender: TObject);
begin

//    ��� ��������� ���������� �� ������ ����� ���������� �������
 ExtCanals();
 massive();
if (not Assigned(Calculat4F)) then   // �������� ������������� ����� (���� ���, ��
  Calculat4F:=TCalculat4F.Create(Self);    // �������� �����)
  Calculat4F.Show; // (��� BrainStrong3F.ShowModal) ����� �����
  BrainStrong3F.Visible:=False;

Memo1.Lines[1]:='����� ������� ��������:';
   Calculat4F.Memo1.Lines.Add('����, �� ������� ������� ���, ������� �������������� � �����, ����� ������� ��� �������������.');
   Calculat4F.Memo1.Lines.Add('������ ���� �������, ������ ���� ��� ��� ����� ������ �������:');
   Calculat4F.Memo1.Lines.Add('���� ��� ����������� ����� ������ ����� ������');
   Calculat4F.Memo1.Lines.Add('��� ����, ��� ��������������� � �����- �� �������������.');
   Calculat4F.Memo1.Lines.Add('����� ������� �� ������ ������: "���c������ �������������� �����"');
   Calculat4F.Memo1.Lines.Add('����� ���������� ������ ������ ������');
   Calculat4F.Memo1.Lines.Add('� � ��������� ���� ����� �������� ����������');
   Calculat4F.StringGrid1.Cells[1,0]:='����� �������';
   Calculat4F.StringGrid1.Cells[0,1]:='��������';
   Calculat4F.StringGrid1.Cells[0,2]:='������';
   Calculat4F.StringGrid1.Cells[0,3]:='�����';
   Calculat4F.StringGrid1.Cells[0,3]:='���������';

   Calculat4F.StringGrid1.Cells[1,1]:= IntToStr(intNum);
   Calculat4F.StringGrid1.Cells[1,2]:= IntToStr(logNum);
   Calculat4F.StringGrid1.Cells[1,3]:= IntToStr(etNum);
   Calculat4F.StringGrid1.Cells[1,4]:= IntToStr(SensNum);
end;



end.
