unit BrainStrong3;

interface

uses
Main, Calculat4, Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TBrainStrong3F = class(TForm)
    HeadPan: TPanel;
    LogHeadLab: TLabel;
    EtHeadLab: TLabel;
    SensHeadLab: TLabel;
    HeadPanel: TLabel;
    NamPan2Lab: TLabel;
    Label2: TLabel;
    LogHeadEd: TEdit;
    IntHeadEd: TEdit;
    EtHeadEd: TEdit;
    SenHeadEd: TEdit;
    Memo1: TMemo;
    Button1: TButton;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure massive();
    procedure ExtCanals();
    procedure Colomn1();
    procedure Colomn2();
    procedure Colomn3();
    procedure Colomn4();
    procedure MakeMemo4form();


  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  BrainStrong3F: TBrainStrong3F;

implementation

{$R *.dfm}

procedure TBrainStrong3F.MakeMemo4form();
begin
  Memo1.Lines[1]:='слева цифрами показано:';
   Calculat4F.Memo1.Lines.Add('Итак, мы сначала описали тим, который взаимействиует с доном, потом описали его наполненность.');
   Calculat4F.Memo1.Lines.Add('Модель наша пробная, потому пока что нет одной важной функции:');
   Calculat4F.Memo1.Lines.Add('пока что возможность учета экстра интро версии');
   Calculat4F.Memo1.Lines.Add('для того, кто взаимодействует с доном- не присутствиует.');
   Calculat4F.Memo1.Lines.Add('После нажатия на нижнюю кнопку: "расcчитать взаимодействие тимов"');
   Calculat4F.Memo1.Lines.Add('будет произведен расчет внутри модели');
   Calculat4F.Memo1.Lines.Add('и в следующем окне будут показаны результаты');

end;

procedure TBrainStrong3F.Colomn3();
begin
   Calculat4F.StringGrid1.Cells[3,0]:= 'мощность';
   Calculat4F.StringGrid1.Cells[3,1]:= IntToStr(int);
   Calculat4F.StringGrid1.Cells[3,2]:= IntToStr(logcan);
   Calculat4F.StringGrid1.Cells[3,3]:= IntToStr(etcan);
   Calculat4F.StringGrid1.Cells[3,4]:= IntToStr(Senscan);
end;

procedure TBrainStrong3F.Colomn4();
begin
   Calculat4F.StringGrid1.Cells[3,0]:= 'мощность';
   Calculat4F.StringGrid1.Cells[2,0]:= 'вход';
   Calculat4F.StringGrid1.Cells[2,1]:= IntToStr(intcan);
   Calculat4F.StringGrid1.Cells[2,2]:= IntToStr(logcan);
   Calculat4F.StringGrid1.Cells[2,3]:= IntToStr(etcan);
   Calculat4F.StringGrid1.Cells[2,4]:= IntToStr(Senscan);
end;
procedure TBrainStrong3F.Colomn2();
begin

   Calculat4F.StringGrid1.Cells[1,0]:='номер аспекта';
   Calculat4F.StringGrid1.Cells[1,1]:= IntToStr(intNum);
   Calculat4F.StringGrid1.Cells[1,2]:= IntToStr(logNum);
   Calculat4F.StringGrid1.Cells[1,3]:= IntToStr(etNum);
   Calculat4F.StringGrid1.Cells[1,4]:= IntToStr(SensNum);
end;


procedure TBrainStrong3F.Colomn1();
begin

   Calculat4F.StringGrid1.Cells[0,1]:='интуиция';
   Calculat4F.StringGrid1.Cells[0,2]:='логика';
   Calculat4F.StringGrid1.Cells[0,3]:='этика';
   Calculat4F.StringGrid1.Cells[0,3]:='сенсорика';
end;


procedure TBrainStrong3F.massive();
begin
  Table[2,intNum] := intHead;
  Table[2,etNum] := etHead;
  Table[2,logNum] := logHead;
  Table[2,sensNum] := senHead;

  Table[3,intNum] := intCan;
  Table[3,etNum] := etCan;
  Table[3,logNum] := logCan;
  Table[3,sensNum] := sensCan;

end;

procedure TBrainStrong3F.ExtCanals();
begin
  intHead  := strtoInt(BrainStrong3F.IntHeadEd.Text);
  logHead  := strtoInt(BrainStrong3F.logHeadEd.Text);
  etHead   := strtoInt(BrainStrong3F.etHeadEd.Text);
  senHead  := strtoInt(BrainStrong3F.senHeadEd.Text);
end;

procedure TBrainStrong3F.Button1Click(Sender: TObject);
begin
   Button1.Visible:=False;
   Button1.Enabled:=False;
   Memo1.Lines[1]:='слева введите:';
   Memo1.Lines.Add('Что приходит внутри головы (дона)?');
   Memo1.Lines.Add('сколько информации может максимально он');
   Memo1.Lines.Add('обработать на каждом аспекте?');
   Memo1.Lines.Add('Например:');
   Memo1.Lines.Add('если ему надо решить математику, то:');
   Memo1.Lines.Add('так как он стандартный дон ( а он может быть нестандартным, у которого наполненность аспекта этики больше, чем логики), то по логике  он может решить в баллах до 10');
   Memo1.Lines.Add('но по этике он плох, сложность 0');
end;

procedure TBrainStrong3F.Button2Click(Sender: TObject);
begin

//    для мощностей внутренних во второй форме заполнение массива
 ExtCanals();
 massive();
if (not Assigned(Calculat4F)) then   // проверка существования Формы (если нет, то
  Calculat4F:=TCalculat4F.Create(Self);    // создание Формы)
  Calculat4F.Show; // (или BrainStrong3F.ShowModal) показ Формы
  BrainStrong3F.Visible:=False;

   MakeMemo4form();
   Colomn1();
   Colomn2();
   Colomn3();

end;



end.
