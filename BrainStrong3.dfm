object BrainStrong3F: TBrainStrong3F
  Left = 0
  Top = 0
  Caption = #1053#1072#1087#1086#1083#1085#1077#1085#1085#1086#1089#1090#1100' '#1072#1089#1087#1077#1082#1090#1086#1074
  ClientHeight = 355
  ClientWidth = 656
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object HeadPan: TPanel
    Left = 8
    Top = 24
    Width = 353
    Height = 189
    ParentBackground = False
    TabOrder = 0
    object LogHeadLab: TLabel
      Left = 8
      Top = 88
      Width = 59
      Height = 13
      Caption = #1051#1086#1075#1080#1095#1077#1089#1082#1080#1081
    end
    object EtHeadLab: TLabel
      Left = 8
      Top = 124
      Width = 54
      Height = 13
      Caption = #1069#1090#1080#1095#1077#1089#1082#1080#1081
    end
    object SensHeadLab: TLabel
      Left = 8
      Top = 159
      Width = 56
      Height = 13
      Caption = #1057#1077#1085#1089#1086#1088#1085#1099#1081
    end
    object HeadPanel: TLabel
      Left = 32
      Top = 16
      Width = 209
      Height = 13
      Caption = '3. '#1052#1086#1097#1085#1086#1089#1090#1100' '#1086#1073#1088#1072#1073#1086#1090#1082#1080' "'#1074#1085#1091#1090#1088#1080' '#1075#1086#1083#1086#1074#1099'"'
    end
    object NamPan2Lab: TLabel
      Left = 104
      Top = 35
      Width = 97
      Height = 13
      Caption = ' '#1085#1072' '#1082#1072#1078#1076#1099#1081' '#1082#1072#1085#1072#1083'?'
    end
    object Label2: TLabel
      Left = 8
      Top = 54
      Width = 69
      Height = 13
      Caption = #1048#1085#1090#1091#1080#1090#1080#1074#1085#1099#1081
    end
    object LogHeadEd: TEdit
      Left = 200
      Top = 94
      Width = 121
      Height = 21
      TabOrder = 0
      Text = '1'
    end
    object IntHeadEd: TEdit
      Left = 200
      Top = 54
      Width = 121
      Height = 21
      TabOrder = 1
      Text = '1'
    end
  end
  object EtHeadEd: TEdit
    Left = 208
    Top = 145
    Width = 121
    Height = 21
    TabOrder = 1
    Text = '1'
  end
  object SenHeadEd: TEdit
    Left = 208
    Top = 180
    Width = 121
    Height = 21
    TabOrder = 2
    Text = '1'
  end
  object Memo1: TMemo
    Left = 374
    Top = 25
    Width = 258
    Height = 234
    Lines.Strings = (
      'Memo1')
    TabOrder = 3
  end
  object Button1: TButton
    Left = 387
    Top = 47
    Width = 194
    Height = 78
    Caption = #1085#1072#1078#1084#1080#1090#1077' '#1076#1083#1103' '#1087#1086#1083#1091#1095#1077#1085#1080#1103' '#1089#1087#1088#1072#1074#1082#1080
    TabOrder = 4
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 304
    Top = 288
    Width = 336
    Height = 47
    Caption = #1076#1072#1083#1077#1077
    TabOrder = 5
    OnClick = Button2Click
  end
end
